import { DockerManagmentService } from './../../docker-managment.service';
import { Component, OnInit, Injectable } from '@angular/core';
import { saveAs, encodeBase64 } from '@progress/kendo-file-saver';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import * as JSZip from 'jszip';

@Component({
  selector: 'app-compose',
  templateUrl: './compose.component.html',
  styleUrls: ['./compose.component.scss']
})
export class ComposeComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  public platformSelected = [];
  public platforms = [];
  public productCodeSelected = 1;
  public products = [];

  constructor(private formBuilder: FormBuilder, private dockerManagmentService: DockerManagmentService) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      targetCentral: [true, [Validators.required]],
      targetLocal: [false, [Validators.required]],
      targetCentralIsSqlServer: [false, [Validators.required]],
      customTargets: [false, [Validators.required]],
      platformSelected: [this.platformSelected, Validators.required],
      hostTargetCentral: ['127.0.0.1', [Validators.required]],
      portTargetCentral: [
        '5000',
        [Validators.required, Validators.minLength(1), Validators.maxLength(6)]
      ],
      hostTargetLocal: ['127.0.0.1', [Validators.required]],
      portTargetLocal: [
        '1433',
        [Validators.required, Validators.minLength(1), Validators.maxLength(6)]
      ]
    });
    this.platforms = [
      {
        text: 'IBM Websphere',
        value: 1,
        productsAvailable: ['CTS', 'CWC', 'IEN'],
        isCustomTargets: true
      },
      {
        text: 'Weblogic 12c',
        value: 2,
        productsAvailable: ['CTS', 'CWC', 'IEN'],
        isCustomTargets: true
      },
      {
        text: 'JBoss EAP 7',
        value: 3,
        productsAvailable: ['CTS', 'CWC', 'IEN'],
        isCustomTargets: true
      },
      {
        text: 'Apache Tomcat',
        value: 4,
        productsAvailable: ['CWC'],
        isCustomTargets: false
      }
    ];

    this.productCodeSelected = 1;
    this.products = [
      {
        text: 'CTS',
        value: 1,
        image: 'logoCTS.png',
        selected: true
      },
      {
        text: 'CWC',
        value: 2,
        image: 'logoCWC.png',
        selected: false
      },
      {
        text: 'IEN',
        value: 3,
        image: 'logoIEN.png',
        selected: false
      }
    ];

    this.platformSelected = this.platforms[0];

    this.dockerManagmentService.getFilesStructure().subscribe(resp => {
      console.log('service', resp);
    });

  }

  onCheckboxChange(product, event) {
      for (let i = 0 ; i < this.products.length; i++) {
        if (this.products[i].text === product.text) {
          this.products[i].selected = event.target.checked;
        }
      }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.downloadFile();
  }

  downloadFile() {
    console.log('download file');
    // const dataURI = 'data:text/plain;base64,' + encodeBase64('Hello World!');
    // saveAs(dataURI, 'test.txt');
    const jszip = new JSZip();
    jszip.file('Hello.txt', 'Hello World\n');

    jszip.generateAsync({ type: 'blob' }).then(function(content) {
      // see FileSaver.js
      saveAs(content, 'example.zip');
    });
  }
}
