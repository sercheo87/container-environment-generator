import { TestBed, inject } from '@angular/core/testing';

import { DockerManagmentService } from './docker-managment.service';

describe('DockerManagmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DockerManagmentService]
    });
  });

  it('should be created', inject([DockerManagmentService], (service: DockerManagmentService) => {
    expect(service).toBeTruthy();
  }));
});
