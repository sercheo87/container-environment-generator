import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DockerManagmentService {

  constructor(private http: HttpClient) { }

  getFilesStructure() {
    return this.http.get('assets/docker-enviroments/enviroments.json');
  }
}
